<?php
/**
 * The Header for our theme.
 *
 * @package Falco
 * @author Muffin group
 * @link http://muffingroup.com
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html <?php language_attributes(); ?>> <!--<![endif]-->

<!-- head -->
<head>



<!-- meta -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<?php if( mfn_opts_get('responsive') ) echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">'; ?>

<title><?php
if( mfn_title() ){
	echo mfn_title();
} else {
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s', 'falco' ), max( $paged, $page ) );
}
?></title>

<?php do_action('wp_seo'); ?>

<link rel="shortcut icon" href="<?php mfn_opts_show('favicon-img',THEME_URI .'/images/favicon.ico'); ?>" type="image/x-icon" />	

<!-- wp_head() -->
<?php wp_head();?>
</head>

<!-- body -->
<body <?php body_class(); ?>>
    


<style>    
    input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}



button:hover {
    opacity: 0.8;
}

/* Extra styles for the cancel button */
.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

/* Center the image and position the close button */
.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
    position: relative;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 10000; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    padding-top: 60px;
    
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* The Close Button (x) */
.close {
    position: absolute;
    right: 25px;
    top: 0;
    color: #000;
    font-size: 35px;
    font-weight: bold;
}
#wppb-login-wrap {margin-top:20px;}
#id01 .homepage_login {
    border: solid 1px #ccc;
    width: 50%;
    margin: 0 auto;
    background: #fff;
}
.close:hover,
.close:focus {
    color: red;
    cursor: pointer;
}
#user_login{
    font-family:byekan;
        height: 44px;
    font-size: 16px;
    width: 100% !important;
    margin-bottom: 10px;
    -webkit-appearance: none;
    background: #fff;
    border: 1px solid #d9d9d9;
    border-top: 1px solid #c0c0c0;
    /* border-radius: 2px; */
    padding: 0 8px;
    box-sizing: border-box;
}
#user_pass {
    font-family:byekan;
     height: 44px;
    font-size: 16px;
    width: 100% !important;
    margin-bottom: 10px;
    -webkit-appearance: none;
    background: #fff;
    border: 1px solid #d9d9d9;
    border-top: 1px solid #c0c0c0;
    /* border-radius: 2px; */
    padding: 0 8px;
    box-sizing: border-box;
}
#wppb-submit { width:9em;}

/* Add Zoom Animation */
.animate {
    -webkit-animation: animatezoom 0.6s;
    animation: animatezoom 0.6s
}

@-webkit-keyframes animatezoom {
    from {-webkit-transform: scale(0)} 
    to {-webkit-transform: scale(1)}
}
    
@keyframes animatezoom {
    from {transform: scale(0)} 
    to {transform: scale(1)}
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
#login_form_close{
    position:absolute;
    top: 10px;
    right:-40px;
}
@media only screen and (max-width: 1000px) {
    #wppb-submit {
        width:100%;
    }
    #login_form_close {
    position: absolute;
    top: 0px;
    right: -26px;
}
}
@media only screen and (max-width: 700px) {
    #id01 .homepage_login {
        width:70%;
    }
    #login_form_close {
    position: absolute;
    top: 0px;
    right: -26px;
}
}
@media only screen and (max-width: 500px) {
    #id01 .homepage_login {
        width:80%;
    }
    #login_form_close {
    top: -19px;
    right: 3px;
}
}
</style>
    
    <div id="id01" class="modal">
       
		<div id="Content" class="homepage_login modal-content animate">
		     
			<div class="content_wrapper clearfix" style="border:5px solid;">
                
				<!-- .sections_group -->
				<div class="sections_group">
				    
					<div class="section the_content"><div class="section_wrapper"><div class="the_content_wrapper">
					    <span id="login_form_close" onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
						<style>
							#pb_login {
								font-family:byekan;
							}
						</style>
							<div id="pb_login"><div id="wppb-login-wrap" class="wppb-user-forms">
								<form name="loginform" id="loginform" action="http://live21.ir/wp-login.php" method="post">
			    <h3 id="login-header-txt">وارد حساب کاربری خود بشوید</h3>

			<p class="login-username">
				
				<input name="log" id="user_login" class="input" value="" size="20" type="text" placeholder="نام کاربری">
			</p>
			<p class="login-password">
				
				<input name="pwd" id="user_pass" class="input" value="" size="20" type="password" placeholder="کلمه عبور">
			</p>
			
			<p class="login-remember"><label><input name="rememberme" id="rememberme" value="forever" type="checkbox"> مرا به خاطر بسپار
</label></p>
			<p class="login-submit">
				<input name="wp-submit" id="wppb-submit" class="button button-primary" value="ورود" type="submit">
				<input name="redirect_to" value="http://live21.ir/?page_id=10214" type="hidden">
				<a class="forgot" href="http://live21.ir/?page_id=10389">کلمه عبور خود را فراموش کرده اید؟</a>
	<a class="forgot" href="http://live21.ir/?page_id=10212">عضویت</a>
			</p>
			
			<input name="wppb_login" value="true" type="hidden"><input name="wppb_form_location" value="page" type="hidden"><input name="wppb_request_url" value="http://live21.ir/?page_id=10214" type="hidden"><input name="wppb_lostpassword_url" value="" type="hidden"><input name="wppb_redirect_priority" value="" type="hidden"><input name="wppb_referer_url" value="http://live21.ir/?page_id=10214" type="hidden">
		</form>
		
		    
		</div></div>
</div></div></div>		</div>
		<script>
// Get the modal
function login_form() {
    document.getElementById("id01").style.display = "block";
}
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>
		<!-- .four-columns - sidebar -->
		

	</div>
</div>
 
    </div>

<script>
// Get the modal
function login_form() {
    document.getElementById("id01").style.display = "block";
}
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
</script>








	
	<!-- #Wrapper -->
	<div id="Wrapper">
		
		<!-- .header_placeholder 4sticky  -->
		<div class="header_placeholder"></div>

		<!-- #Header -->
		<header id="Header">
		
			<?php 
				if( mfn_opts_get('header-email') ||  mfn_opts_get('header-phone') ){
					echo '<div id="top_bar">';
						if( $header_email = mfn_opts_get('header-email') ){
							echo '<p class="mob_phone"><i class="icon-envelope-alt"></i><a href="mailto:'. $header_email .'">'. $header_email .'</a></p>';
						}
						if( $header_phone = mfn_opts_get('header-phone') ){
							echo '<p class="mob_mail"><i class="icon-phone"></i><a href="tel:'. $header_phone .'">'. $header_phone .'</a></p>';
						}
					echo '</div>';
				}
			?>

			<div class="container">
				<div class="column one">
				
					<!-- .logo -->
					<div class="logo">
						<?php if( is_front_page() ) echo '<h1>'; ?>
						<a id="logo" href="<?php echo home_url(); ?>" title="<?php bloginfo( 'name' ); ?>">
							<img class="scale-with-grid" src="<?php mfn_opts_show('logo-img',THEME_URI .'/images/logo.png'); ?>" alt="<?php bloginfo( 'name' ); ?>" />
						</a>
						<?php if( is_front_page() ) echo '</h1>'; ?>
					</div>
					
					<!-- .social -->
					<div class="social">
						<ul>
							<?php if( mfn_opts_get('social-facebook') ): ?><li class="facebook"><a target="_blank" href="<?php mfn_opts_show('social-facebook'); ?>" title="Facebook">F</a></li><?php endif; ?>
							<?php if( mfn_opts_get('social-googleplus') ): ?><li class="googleplus"><a target="_blank" href="<?php mfn_opts_show('social-googleplus'); ?>" title="Google+">G</a></li><?php endif; ?>
							<?php if( mfn_opts_get('social-twitter') ): ?><li class="twitter"><a target="_blank" href="<?php mfn_opts_show('social-twitter'); ?>" title="Twitter">L</a></li><?php endif; ?>
							<?php if( mfn_opts_get('social-vimeo') ): ?><li class="vimeo"><a target="_blank" href="<?php mfn_opts_show('social-vimeo'); ?>" title="Vimeo">V</a></li><?php endif; ?>
							<?php if( mfn_opts_get('social-youtube') ): ?><li class="youtube"><a target="_blank" href="<?php mfn_opts_show('social-youtube'); ?>" title="YouTube">X</a></li><?php endif; ?>
							<?php if( mfn_opts_get('social-flickr') ): ?><li class="flickr"><a target="_blank" href="<?php mfn_opts_show('social-flickr'); ?>" title="Flickr">N</a></li><?php endif; ?>
							<?php if( mfn_opts_get('social-linkedin') ): ?><li class="linked_in"><a target="_blank" href="<?php mfn_opts_show('social-linkedin'); ?>" title="LinkedIn">I</a></li><?php endif; ?>
							<?php if( mfn_opts_get('social-pinterest') ): ?><li class="pinterest"><a target="_blank" href="<?php mfn_opts_show('social-pinterest'); ?>" title="Pinterest">:</a></li><?php endif; ?>
							<?php if( mfn_opts_get('social-dribbble') ): ?><li class="dribbble"><a target="_blank" href="<?php mfn_opts_show('social-dribbble'); ?>" title="Dribbble">D</a></li><?php endif; ?>							
						</ul>
					</div>
					
					<div class="addons">
						
						<?php $translate['search-placeholder'] = mfn_opts_get('translate') ? mfn_opts_get('translate-search-placeholder','Enter your search') : __('Enter your search','falco'); ?>
						<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<a class="icon" href="#"><i class="icon-search"></i></a>
							<input type="text" class="field" name="s" id="s" placeholder="<?php echo $translate['search-placeholder']; ?>" />
							<input type="submit" class="submit" value="" style="display:none;" />
						</form>
						
						<?php if( has_nav_menu( 'lang-menu' ) ): ?>
							<div class="language">
								<a href="#"><i class="icon-globe"></i><?php echo mfn_get_menu_name( 'lang-menu' ); ?></a>
								<div class="language_select">
									<?php mfn_wp_lang_menu(); ?>
								</div>
							</div>
						<?php endif; ?>
						
						<?php 
							if( $header_email = mfn_opts_get('header-email') ){
								echo '<div class="mail expand"><i class="icon-envelope-alt"></i> <p class="label"><a href="mailto:'. $header_email .'">'. $header_email .'</a></p></div>';
							}
							if( $header_phone = mfn_opts_get('header-phone') ){
								echo '<div class="phone expand"><i class="icon-phone"></i> <p class="label"><a href="tel:'. $header_phone .'">'. $header_phone .'</a></p></div>';
							}
						?>

					</div>
					
					<!-- #menu -->
					<?php mfn_wp_nav_menu(); ?>	
					<a class="responsive-menu-toggle" href="#"><i class='icon-reorder'></i></a>

				</div>		
			</div>
			
		</header>
		
		<?php 
			if( ! is_404() ){					
				
				$slider = false;
				if( get_post_type()=='page' ) $slider = get_post_meta( get_the_ID(), 'mfn-post-slider', true );				
				
				if( $slider ){
					
					if( $slider == 'mfn-slider' ){

						// Muffin Slider
						get_template_part( 'includes/header', 'mfn-slider' );
						
					} elseif( function_exists( 'putRevSlider' ) ){

						// Revolution Slider
						echo '<div id="mfn-rev-slider">';
							putRevSlider( $slider );
						echo '</div>';
						
					}
					
				} elseif( trim( wp_title( '', false ) ) ){

					// Page title
					echo '<div id="Subheader">';
						echo '<div class="container">';
							echo '<div class="column one">';
								if( get_post_type()=='page' || is_single() ){
									echo '<h1 class="title">'. $post->post_title .'</h1>';
								} elseif( is_search() ){
									global $wp_query;
									$total_results = $wp_query->found_posts;
									echo '<h1 class="title">'. $total_results .' results found for: '. get_search_query() .'</h1>';
								} else {
									echo '<h1 class="title">'. trim( wp_title( '', false ) ) .'</h1>';
								}
								mfn_breadcrumbs();
							echo '</div>';
						echo '</div>';
					echo '</div>';
					
				}
				
			} else {

				// Error 404
				echo '<div id="Subheader">';
					echo '<div class="container">';
						echo '<div class="column one">';
							echo '<h1 class="title">'. __( 'Error 404', 'falco' ) .'</h1>';
						echo '</div>';
					echo '</div>';
				echo '</div>';
	
			}
		?>