<style>
    body{
        direction: rtl;
    }

    .col3{
        width: 25%;
        float: right;
        box-sizing: border-box;
    }

    .live{
        display: block;
        width: 97%;
        margin: 0.5em auto;
        height: 232px;
        box-shadow: 1px 2px 9px 0px #dcdcdc;
        box-sizing: border-box;
        text-decoration: blink;
        position: relative;

    }
    .live:hover{
       box-shadow: 1px 2px 9px 0px #928b8b;
s
    }
    .live::before{
        content: "\25B6";
        display: block;
        width: 40px;
        height: 40px;
        position: absolute;
        top: 50%;
        left: 50%;
        margin: -20px 0 0 -18px;
        border-radius: 50%;
        background: transparent;
        text-align: center;
        font: 24px/36px 'Glyphicons Halflings';
        transform: scale(.8);
        opacity: 0;
                -webkit-transition: opacity .2s ease;
        transition: opacity .2s ease;
        text-decoration: none !important;
        box-shadow: 0 0 0 2px rgba(255, 255, 255, 1);
        color: #fff;
    }
     .live:hover::before {
        -webkit-transform: scale(1);
        transform: scale(1);
        opacity: 1;
        text-decoration: none;
        z-index:1;
        text-decoration: none !important;

     }
     .live > div {
        position:relative;
     }
     .live > div:before {
         content: '';
         position: absolute;
         top:0;
         bottom:0;
         left:0;
         right:0;
     }
     .live:hover > div:before{
         background : rgba(0,0,0,0.4);
     }
    .live img{
        width: 100%;
        height: 200px!important;
        display: block;
    }
    .live span{
        text-align: center;
        display: block;
        white-space: nowrap;
        overflow: hidden;
        width:calc(100% - 6px);
        margin: 5px auto;
        color: #111;
    }
    @media (max-width: 1024px) 
    {
        .col3 {
            width:33.33333%;
        }
    }
     @media (max-width: 768px) 
     {
        .col3 {
            width:50%;
        }
    }
    @media (max-width: 500px) 
    {
        .col3 {
             width: 100%;
        }
    }
    
    
     @media (max-width: 400px) {
        .col3 {
            width:100%;
        }
    }

</style>
<?php
    function getData($api) {
        // Get cURL resource
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $api,
            CURLOPT_CUSTOMREQUEST=> "GET",
            CURLOPT_HTTPHEADER=> array(
            'Content-Type: application/json'
            )
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        return $resp;
    }


    $live21 =  getData('http://ir1.live21.ir/admin/return_online.php');
    $live21=json_decode($live21,true);
    //var_dump($live21);
     $j =  sizeof($live21);
    //$j = (false) ? 6 : sizeof($live21);
    for($i=0; $i<$j; $i++){
        $json_link = getData($live21[$i]."/info.json");
        $json_link = json_decode($json_link, true);
    ?>

        <div class="col3">
            <a href="<?php echo $live21[$i] ?>" target="_blank" class="live">
                <div>
                    <img src="https://ir1.live21.ir/official/<?php echo     $json_link['username'] ?>/<?php echo $json_link['pic'] ?>" alt="<?php echo $json_link['about'] ?>" />
                </div>
                <span><?php echo $json_link['about'] ?></span>
            </a>
        </div>
    <?php
    }
?>



