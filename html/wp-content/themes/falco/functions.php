<?php
/**
 * Theme Functions
 *
 * @package Falco
 * @author Muffin group
 * @link http://muffingroup.com
 */

/*-------------------------------ADD CSS file To ALL Page Admin------------------------------------------------------*/
function load_custom_wp_admin_style() {     
   wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/admin-rtl-farishtheme.css', false, '1.0.0' );   
     wp_enqueue_style( 'custom_wp_admin_css' );}
	 add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
/*-----------------------------------------------------------------------------------*/
define( 'THEME_DIR', get_template_directory() );
define( 'THEME_URI', get_template_directory_uri() );

define( 'THEME_NAME', 'Falco' );
define( 'THEME_VERSION', '1.2.8' );

define( 'LIBS_DIR', THEME_DIR. '/functions' );
define( 'LIBS_URI', THEME_URI. '/functions' );
define( 'LANG_DIR', THEME_DIR. '/languages' );

add_filter( 'widget_text', 'do_shortcode' );


/* ---------------------------------------------------------------------------
 * Loads Theme Textdomain
 * --------------------------------------------------------------------------- */
load_theme_textdomain( 'falco', LANG_DIR );
load_theme_textdomain( 'mfn-opts', LANG_DIR );


/* ---------------------------------------------------------------------------
 * Loads the Options Panel
 * --------------------------------------------------------------------------- */
function mfn_admin_scripts() {
// 	wp_enqueue_script( 'jquery-ui-droppable' );
	wp_enqueue_script( 'jquery-ui-sortable' );
}    
add_action( 'wp_enqueue_scripts', 'mfn_admin_scripts' );
add_action( 'admin_enqueue_scripts', 'mfn_admin_scripts' );
	
require( THEME_DIR .'/muffin-options/theme-options.php' );


/* ---------------------------------------------------------------------------
 * Loads Theme Functions
 * --------------------------------------------------------------------------- */

// Functions --------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-functions.php' );

// Header -----------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-head.php' );

// Menu -------------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-menu.php' );

// Meta box ---------------------------------------------------------------------
require_once( LIBS_DIR .'/meta-functions.php' );
require_once( LIBS_DIR .'/meta-page.php' );
require_once( LIBS_DIR .'/meta-post.php' );

// Custom post types ------------------------------------------------------------
require_once( LIBS_DIR .'/meta-client.php' );
require_once( LIBS_DIR .'/meta-offer.php' );
require_once( LIBS_DIR .'/meta-portfolio.php' );
require_once( LIBS_DIR .'/meta-testimonial.php' );
require_once( LIBS_DIR .'/meta-slide.php' );

// Shortcodes -------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-shortcodes.php' );

// Widgets ----------------------------------------------------------------------
require_once( LIBS_DIR .'/widget-functions.php' );

require_once( LIBS_DIR .'/widget-company.php' );
require_once( LIBS_DIR .'/widget-flickr.php' );
require_once( LIBS_DIR .'/widget-menu.php' );
require_once( LIBS_DIR .'/widget-recent-comments.php' );
require_once( LIBS_DIR .'/widget-recent-posts.php' );
require_once( LIBS_DIR .'/widget-tag-cloud.php' );

// TinyMCE ----------------------------------------------------------------------
require_once( LIBS_DIR .'/tinymce/tinymce.php' );

// Plugins ---------------------------------------------------------------------- 
require_once( LIBS_DIR .'/class-tgm-plugin-activation.php' );

// Hide activation and update specific parts ------------------------------------

// Slider Revolution
if( function_exists( 'set_revslider_as_theme' ) ){
	set_revslider_as_theme();
}
function live_func( $atts ) {
   $a = shortcode_atts( array(
       'count' => 6,
   ), $atts );

ob_start(); // turn on output buffering
include('live.php');
$live = ob_get_contents(); // get the contents of the output buffer
ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering
   return $live;
}
add_shortcode( 'live', 'live_func' );


function live2_func( $atts ) {


ob_start(); // turn on output buffering
include('live2.php');
$live = ob_get_contents(); // get the contents of the output buffer
ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering
   return $live;
}
add_shortcode( 'live2', 'live2_func' );





function live3_func( $atts ) {


ob_start(); // turn on output buffering
include('live3.php');
$live = ob_get_contents(); // get the contents of the output buffer
ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering
   return $live;
}
add_shortcode( 'live3', 'live3_func' );














function search_func( $atts ) {

ob_start(); // turn on output buffering
include('search2.php');
$search_result = ob_get_contents(); // get the contents of the output buffer
ob_end_clean(); //  clean (erase) the output buffer and turn off output buffering
   return $search_result;
}
add_shortcode( 'search_result', 'search_func' );






?>