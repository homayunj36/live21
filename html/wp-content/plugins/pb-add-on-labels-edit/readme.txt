== Profile Builder - Labels Edit Add-On ==

Contributors: cozmoslabs, cristophor22
Author URL: http://cozmoslabs.com/
Donate link: http://www.cozmoslabs.com/
Tags: labels, edit, labels edit, strings, pb, front-end, back-end, profile builder
Stable version: 1.0.9

This plugin adds a subpage where you can edit labels of Profile Builder.


== Description ==

Profile Builder - Labels Edit Add-On

This plugin adds a subpage where you can edit labels of Profile Builder.


== Installation ==

1. Upload and install the zip file via the built in WordPress plugin installer.
2. Activate the plugin from the "Plugins" admin panel using the "Activate" link.


== Changelog ==

= 1.0.1 =
Added strings from userlisting.php to editable labels.

= 1.0.2 =
Added Import and Export labels to .json file.

= 1.0.3 =
Changed some functions to make add-on compatible with older PHP versions.

= 1.0.5 =
Added missing strings.

= 1.0.6 =
Bug fixes.

= 1.0.7 =
Fixed: When adding a new label, the original is not being displayed.
Fixed: Modifying strings with % doesn't work at all.
Fixed: Change the priority for gettext filter to 8 so it works with WPML.
Refactor: Added standalone WCK-API library in order to fix strings with % issue.

= 1.0.8 =
Plugin security improvements.

= 1.0.9 =
Bug fixes.